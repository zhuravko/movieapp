package com.example.movieapp.tools;

public class Constants {
    public static final String USER_DATA_STORAGE_NAME = "USER_DATA";
    public static final String USER_DATA_STORAGE_MOVIES = "movies";
    public static final String IMAGE_PATH = "https://image.tmdb.org/t/p/w500";
}
