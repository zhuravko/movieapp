package com.example.movieapp.localdata;

import android.content.Context;
import android.content.SharedPreferences;
import com.example.movieapp.tools.Constants;

import java.util.Set;

public class UserInfo {
    private Set<String> movies;
    private SharedPreferences preferences;

    public UserInfo(Context context) {
        preferences = context.getSharedPreferences(Constants.USER_DATA_STORAGE_NAME, Context.MODE_PRIVATE);
        movies = preferences.getStringSet(Constants.USER_DATA_STORAGE_MOVIES, null);
    }

    public Set<String> getMovies() {
        return movies;
    }

    public void setMovies(Set<String> movies) {
        this.movies = movies;
        putStringSet(Constants.USER_DATA_STORAGE_MOVIES, movies);
    }
    private void putStringSet(String key, Set<String> value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(key, value);
        editor.apply();
    }
}
