package com.example.movieapp.network.apimethods;

import com.example.movieapp.network.apimodels.GetMovies;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieAPI {

    @GET("https://api.themoviedb.org/3/discover/movie?language=ru")
    Call<GetMovies> getMovies(@Query("api_key") String apiKey, @Query("page") int page);
}
