package com.example.movieapp.network;

import com.example.movieapp.network.apimethods.MovieAPI;
import com.example.movieapp.network.apimodels.GetMovies;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieAPIFunctions {
    private static final String API_KEY = "6ccd72a2a8fc239b13f209408fc31c33";
    public void getMovies(int page, final OnDataLoadContract<GetMovies> contract) {
        MovieAPI api = NetworkService.getInstance().getRetrofit().create(MovieAPI.class);
        api.getMovies(API_KEY, page).enqueue(new Callback<GetMovies>() {
            @Override
            public void onResponse(Call<GetMovies> call, Response<GetMovies> response) {
                contract.onGetDataNetworkSuccess(call, response);
            }

            @Override
            public void onFailure(Call<GetMovies> call, Throwable t) {
                contract.onGetDataNetworkFailure(call, t);
            }
        });
    }
}
