package com.example.movieapp.network;

import retrofit2.Call;
import retrofit2.Response;

public interface OnDataLoadContract<T> {
    void onGetDataNetworkSuccess(Call<T> call, Response<T> response);
    void onGetDataNetworkFailure(Call<T> call, Throwable t);
}