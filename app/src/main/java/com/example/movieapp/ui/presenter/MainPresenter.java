package com.example.movieapp.ui.presenter;

import android.app.Activity;
import android.widget.Toast;
import com.example.movieapp.R;
import com.example.movieapp.network.apimodels.GetMovies;
import com.example.movieapp.network.apimodels.Result;
import com.example.movieapp.tools.NetTools;
import com.example.movieapp.ui.contracts.MainContract;
import com.example.movieapp.ui.repository.MainRepository;
import retrofit2.Call;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class MainPresenter implements MainContract.Presenter {
    
    private MainContract.Repository repository;
    private MainContract.View view;
    private List<Result> currentMovies = new ArrayList<>();

    public MainPresenter(MainContract.View view) {
        this.view = view;
        repository = new MainRepository(this);
    }
    @Override
    public void getMovies(){
        if(view instanceof Activity){
            Activity activity = (Activity) view;
            if(NetTools.isOnline(activity)){
                repository.getMovies(view.getPage());
            }
            else {
                if(view.getPage() == 1) {
                    Toast.makeText(activity, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    view.setVisibleBlockTryAgain(true);
                }
                else {
                    Toast.makeText(activity, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    view.setVisibleRefreshLoadingHeader(false);
                }
            }
        }

    }

    @Override
    public void onNickTextChanged(String s){
        List<Result> temp = new ArrayList<>();
        for (Result movie : currentMovies) {
            if (movie.getTitle().toLowerCase().contains(s.toLowerCase())) {
                temp.add(movie);
            }
        }
        if(temp.size() != 0){
            view.setVisibleNotFound(false, null);
            view.setMovies(temp);
        }else {
            view.setVisibleNotFound(true, s);
        }
        view.setVisibleRefreshLoadingHeader(false);
    }
    @Override
    public List<Result> getLocalMovies(){
        return currentMovies;
    }

    @Override
    public void onGetMoviesSuccess(Call<GetMovies> call, Response<GetMovies> response){
        view.setVisibleBlockTryAgain(false);
        view.setVisibleRefreshLoadingHeader(false);
        if (response.body() != null){
            if(view.getPage() == 1) {
                view.setMovies(response.body().getResults());
                currentMovies = response.body().getResults();
            }
            else {
                view.addMovies(response.body().getResults());
                currentMovies.addAll(response.body().getResults());
            }
        }

    }

    @Override
    public void setVisibleRefreshLoadingHeader(boolean state){
        view.setVisibleRefreshLoadingHeader(state);
    }

    @Override
    public void onGetMoviesFailure(Call<GetMovies> call, Throwable t){
        if(view instanceof Activity) {
            Activity activity = (Activity) view;
            if (view.getPage() == 1)
                view.setVisibleBlockTryAgain(true);
            else {
                Toast.makeText(activity, R.string.no_internet, Toast.LENGTH_SHORT).show();
                view.setVisibleRefreshLoadingHeader(false);
            }
        }
    }
}
