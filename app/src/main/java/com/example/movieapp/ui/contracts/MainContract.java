package com.example.movieapp.ui.contracts;

import com.example.movieapp.network.apimodels.GetMovies;
import com.example.movieapp.network.apimodels.Result;
import retrofit2.Call;
import retrofit2.Response;

import java.util.List;

public interface MainContract {
    interface View{
        int getPage();
        void setMovies(List<Result> movies);
        void addMovies(List<Result> movies);
        void setVisibleNotFound(boolean state, String s);
        void setVisibleBlockTryAgain(boolean state);
        void setVisibleRefreshLoadingHeader(boolean state);
    }
    interface Presenter{
        void getMovies();
        List<Result> getLocalMovies();
        void onNickTextChanged(String s);
        void setVisibleRefreshLoadingHeader(boolean state);
        void onGetMoviesSuccess(Call<GetMovies> call, Response<GetMovies> response);
        void onGetMoviesFailure(Call<GetMovies> call, Throwable t);
    }
    interface Repository{
        void getMovies(int page);
    }
}
