package com.example.movieapp.ui.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.movieapp.R;
import com.example.movieapp.network.apimodels.Result;
import com.example.movieapp.tools.Constants;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MovieActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        ImageView imageMovie = findViewById(R.id.image_movie);
        TextView title = findViewById(R.id.title);
        TextView description = findViewById(R.id.description);
        TextView date = findViewById(R.id.date_text);


        Result movie = getIntent().getParcelableExtra("movie");

        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(movie.getTitle());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        title.setText(movie.getTitle());
        description.setText(movie.getOverview());
        if (movie.getReleaseDate() != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateMovie = sdf.parse(movie.getReleaseDate());
                sdf = new SimpleDateFormat("d MMMM yyyy");
                date.setText(sdf.format(dateMovie));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Picasso.with(this)
                .load(Constants.IMAGE_PATH + movie.getPosterPath())
                .fit()
                .centerCrop()
                .into(imageMovie);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
