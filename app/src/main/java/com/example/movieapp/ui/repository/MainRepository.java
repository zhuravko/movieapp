package com.example.movieapp.ui.repository;

import com.example.movieapp.network.MovieAPIFunctions;
import com.example.movieapp.network.OnDataLoadContract;
import com.example.movieapp.network.apimodels.GetMovies;
import com.example.movieapp.ui.contracts.MainContract;
import retrofit2.Call;
import retrofit2.Response;

public class MainRepository implements MainContract.Repository {
    MainContract.Presenter presenter;

    public MainRepository(MainContract.Presenter presenter){
        this.presenter = presenter;
    }
    @Override
    public void getMovies(int page){
        MovieAPIFunctions movieAPIFunctions = new MovieAPIFunctions();
        movieAPIFunctions.getMovies(page, new OnDataLoadContract<GetMovies>() {
            @Override
            public void onGetDataNetworkSuccess(Call<GetMovies> call, Response<GetMovies> response) {
                presenter.onGetMoviesSuccess(call,response);
            }

            @Override
            public void onGetDataNetworkFailure(Call<GetMovies> call, Throwable t) {
                presenter.onGetMoviesFailure(call,t);
            }
        });
    }
}
