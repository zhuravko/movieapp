package com.example.movieapp.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Outline;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.movieapp.R;
import com.example.movieapp.localdata.UserInfo;
import com.example.movieapp.network.apimodels.Result;
import com.example.movieapp.tools.Constants;
import com.example.movieapp.ui.activities.MovieActivity;
import com.example.movieapp.ui.contracts.MainContract;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Result> movies;
    private List<String> favorites;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 0;
    private MainContract.Presenter presenter;


    public MoviesAdapter(Context context) {
        this.context = context;
        movies = new ArrayList<>();
    }
    public MoviesAdapter(Context context, List<Result> list, MainContract.Presenter presenter) {
        this.context = context;
        movies = list;
        this.presenter = presenter;
        UserInfo userInfo = new UserInfo(context);
        if(userInfo.getMovies()!=null)
            favorites = new ArrayList<>(userInfo.getMovies());
        else
            favorites = new ArrayList<>();
        for (Result movie : movies) {
            for (String favorite : favorites) {
                if(movie.getTitle().equals(favorite))
                    movie.setFavorite(true);
            }
        }
    }

    public void addMovies(List<Result> list) {
        movies.addAll(list);
        for (Result movie : list) {
            for (String favorite : favorites) {
                if(movie.getTitle().equals(favorite))
                    movie.setFavorite(true);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(position == getItemCount() - 1 && movies.size() != 0)
            return TYPE_FOOTER;
        return TYPE_ITEM;
    }

    @NonNull
    @Override
    public MoviesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == TYPE_ITEM) {
            View v = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_movie, parent, false);
            MoviesAdapter.ViewHolder holder = new MoviesAdapter.ViewHolder(v);
            return holder;
        }else {
            View v = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_movies, parent, false);
            MoviesAdapter.ViewHolder holder = new MoviesAdapter.ViewHolder(v);
            return holder;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        View v = holder.itemView;
        if (position < getItemCount() - 1) {
            final Result movie = movies.get(position);
            ImageView imageMovie = v.findViewById(R.id.image_movie);
            ImageView imageLike = v.findViewById(R.id.icon_heart);
            TextView title = v.findViewById(R.id.title);
            TextView description = v.findViewById(R.id.description);
            TextView date = v.findViewById(R.id.date_text);

            title.setText(movie.getTitle());
            description.setText(movie.getOverview());
            if (movie.getReleaseDate() != null) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateMovie = sdf.parse(movie.getReleaseDate());
                    sdf = new SimpleDateFormat("d MMMM yyyy");
                    date.setText(sdf.format(dateMovie));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (movie.isFavorite()) {
                imageLike.setImageResource(R.drawable.ic_heart_fill);
            } else {
                imageLike.setImageResource(R.drawable.ic_heart);
            }

            LinearLayout userLine = v.findViewById(R.id.movie_item);

            userLine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MovieActivity.class);
                    intent.putExtra("movie", movie);
                    context.startActivity(intent);
                }
            });
            imageLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (movie.isFavorite()) {
                        favorites.remove(movie.getTitle());
                        imageLike.setImageResource(R.drawable.ic_heart);
                        movie.setFavorite(false);
                    } else {
                        favorites.add(movie.getTitle());
                        imageLike.setImageResource(R.drawable.ic_heart_fill);
                        movie.setFavorite(true);
                    }
                    UserInfo userInfo = new UserInfo(context);
                    userInfo.setMovies(new HashSet<>(favorites));
                }
            });
            imageMovie.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setRoundRect(0, 0, view.getWidth(), view.getHeight() + 25, 18);
                }
            });
            imageMovie.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setRoundRect(0, 0, view.getWidth() + 25, view.getHeight(), 18);
                }
            });
            imageMovie.setClipToOutline(true);
            Picasso.with(context)
                    .load(Constants.IMAGE_PATH + movie.getPosterPath())
                    .fit()
                    .centerCrop()
                    .into(imageMovie);
        }else if (holder.getItemViewType() == TYPE_FOOTER){
            ImageButton button = v.findViewById(R.id.button_refresh);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.setVisibleRefreshLoadingHeader(true);
                    presenter.getMovies();
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return movies.size() == 0 ? 0 : movies.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}