package com.example.movieapp.ui.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.example.movieapp.R;
import com.example.movieapp.network.apimodels.Result;
import com.example.movieapp.ui.adapters.MoviesAdapter;
import com.example.movieapp.ui.contracts.MainContract;
import com.example.movieapp.ui.presenter.MainPresenter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainContract.Presenter presenter;
    private RecyclerView list;
    private MoviesAdapter adapter;
    private EditText search;
    private ProgressBar progressBar;
    private ProgressBar progressBarHeader;
    private LinearLayout blockTryAgain;
    private LinearLayout blockNotFound;
    private ImageButton buttonRefresh;
    private boolean isNewItemsLoading = false;
    private TextView textSearch;
    private int page = 1;
    private LinearLayoutManager linearLayoutManager;
    private int firstVisibleItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainPresenter(this);
        SwipeRefreshLayout refreshLayout = findViewById(R.id.refresh);
        list = findViewById(R.id.list_movies);
        search = findViewById(R.id.search);
        progressBar = findViewById(R.id.progress_bar);
        progressBarHeader = findViewById(R.id.progress_bar_header);
        blockTryAgain = findViewById(R.id.block_try_again);
        blockNotFound = findViewById(R.id.block_not_found);
        buttonRefresh = findViewById(R.id.button_refresh);
        textSearch = findViewById(R.id.text_search);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        presenter.getMovies();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setVisibleRefreshLoadingHeader(true);
                presenter.onNickTextChanged(s.toString());
            }
        });
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                search.setText("");
                setVisibleNotFound(false, null);
                presenter.getMovies();
                refreshLayout.setRefreshing(false);
            }
        });
        buttonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
                blockTryAgain.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                presenter.getMovies();
            }
        });
        linearLayoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(linearLayoutManager);
        progressBar.setVisibility(View.VISIBLE);
        list.setAdapter(adapter);
        list.scrollToPosition(firstVisibleItems);
        list.smoothScrollToPosition(firstVisibleItems);
        list.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();     //смотрим сколько элементов на экране
                int totalItemCount = linearLayoutManager.getItemCount();        //сколько всего элементов
                firstVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();     //какая позиция первого элемента
                if (firstVisibleItems >= totalItemCount -  5 && !isNewItemsLoading && list.getAdapter().getItemCount() >= 20) {
                    isNewItemsLoading = true;
                    progressBarHeader.setVisibility(View.VISIBLE);
                    page += 1;
                    presenter.getMovies();
                }
            }
        });

    }

    @Override
    public void setMovies(List<Result> movies){
        adapter = new MoviesAdapter(MainActivity.this, movies, presenter);
        progressBar.setVisibility(View.GONE);
        list.setAdapter(adapter);

    }
    @Override
    public void addMovies(List<Result> movies){
        adapter.addMovies(movies);
        progressBarHeader.setVisibility(View.GONE);
        isNewItemsLoading = false;
    }
    @Override
    public void setVisibleNotFound(boolean state, String s){
        if(state){
            if(!s.isEmpty()) {
                list.setVisibility(View.GONE);
                blockNotFound.setVisibility(View.VISIBLE);
                textSearch.setText("По запросу " + (char) 34 + s + (char) 34 + " ничего не найдено");
            }
        }else {
            list.setVisibility(View.VISIBLE);
            blockNotFound.setVisibility(View.GONE);
        }
    }

    @Override
    public void setVisibleRefreshLoadingHeader(boolean state){
        if(state)
            progressBarHeader.setVisibility(View.VISIBLE);
        else
            progressBarHeader.setVisibility(View.GONE);

    }

    @Override
    public void setVisibleBlockTryAgain(boolean state){
        if(state) {
            list.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            blockTryAgain.setVisibility(View.VISIBLE);
        }
        else {
            list.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            blockTryAgain.setVisibility(View.GONE);
        }
    }
    @Override
    public int getPage(){
        return page;
    }
}